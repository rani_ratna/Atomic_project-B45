-- phpMyAdmin SQL Dump
-- version 4.5.1
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: Feb 06, 2017 at 08:36 PM
-- Server version: 10.1.13-MariaDB
-- PHP Version: 7.0.8

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `atomic_project_b45`
--

-- --------------------------------------------------------

--
-- Table structure for table `birth_day`
--

CREATE TABLE `birth_day` (
  `id` int(11) NOT NULL,
  `name` varchar(111) COLLATE utf8_croatian_ci NOT NULL,
  `birth_day` varchar(111) COLLATE utf8_croatian_ci NOT NULL,
  `soft_delete` varchar(11) COLLATE utf8_croatian_ci NOT NULL DEFAULT 'no'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_croatian_ci;

--
-- Dumping data for table `birth_day`
--

INSERT INTO `birth_day` (`id`, `name`, `birth_day`, `soft_delete`) VALUES
(1, 'shuvo', '1997-02-25', 'no'),
(2, 'apu', '2017-01-11', 'no'),
(3, 'rupen', '2017-01-13', 'no'),
(4, 'shely', '2017-01-04', 'no'),
(5, 'ola', '2017-01-04', 'no'),
(6, 'ola', '2017-01-04', 'no'),
(7, 'ola', '2017-01-04', 'no'),
(8, 'ula', '2017-02-23', 'no'),
(9, 'ula', '2017-02-23', 'no'),
(10, 'lala', '2017-01-05', 'no'),
(11, 'lala', '2017-01-05', 'no'),
(12, 'lala', '2017-01-05', 'no'),
(13, 'lala', '2017-01-05', 'no'),
(14, 'lala', '2017-01-05', 'no'),
(15, 'lala', '2017-01-05', 'no'),
(16, 'lala', '2017-01-05', 'no'),
(17, 'lala', '2017-01-05', 'no'),
(18, 'lala', '2017-01-05', 'no'),
(19, 'lala', '2017-01-05', 'no'),
(20, 'lala', '2017-01-05', 'no'),
(21, 'lala', '2017-01-10', 'no'),
(23, 'shuvo', '2017-02-17', 'no'),
(24, 'apu', '2017-02-03', 'no'),
(25, 'asdflkhj', '2017-06-13', 'no'),
(26, 'adsfasdf', '2017-03-01', 'no'),
(27, ',zkjnxhcvjk', '2017-02-14', 'no'),
(29, 'dfgdfg', '2017-02-10', 'no'),
(30, 'plkmk', '2017-02-09', 'no');

-- --------------------------------------------------------

--
-- Table structure for table `book_title`
--

CREATE TABLE `book_title` (
  `id` int(11) NOT NULL,
  `book_name` varchar(111) COLLATE utf8_croatian_ci NOT NULL,
  `author_name` varchar(111) COLLATE utf8_croatian_ci NOT NULL,
  `soft_delete` varchar(11) COLLATE utf8_croatian_ci NOT NULL DEFAULT 'no'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_croatian_ci;

--
-- Dumping data for table `book_title`
--

INSERT INTO `book_title` (`id`, `book_name`, `author_name`, `soft_delete`) VALUES
(1, '', '', 'no'),
(2, 'swidesh', 'shuvo', 'no'),
(4, '', '', 'no'),
(6, 'kocori', 'shuvo', 'no'),
(7, 'kocori', 'shuvo', 'no'),
(8, 'kotkoti', 'shuvo', 'no'),
(9, 'ali baba', 'shuvo', 'no'),
(10, '40 cor', 'shuvo', 'no'),
(11, 'bhutu', 'shuvo', 'no'),
(12, 'de tali', 'shuvo', 'no'),
(13, 'kacara', 'shuvo', 'no'),
(14, 'tala doba kocuri', 'shuvo', 'no'),
(15, 'facebook theke prem', 'shuvo', 'no'),
(16, 'kaptai bad', 'shuvo', 'no'),
(17, 'swidesh er lal deoal', 'shuvo', 'no'),
(18, 'school amar vhallage na', 'shuvo', 'no'),
(19, 'BITM theke suru', 'shuvo', 'no'),
(20, 'jonyr chera jainga', 'shuvo', 'no'),
(21, 'html dia suru', 'shuvo', 'no'),
(22, 'ulta palta cola', 'shuvo', 'no'),
(23, 'lukia sigaret khaoa', 'shuvo', 'no'),
(24, 'ma er hater ador', 'shuvo', 'no'),
(25, 'messenger er tin tin aoyag', 'shuvo', 'no'),
(26, 'clg er first crush', 'shuvo', 'no'),
(27, 'first crush palia gelo bf er sate', 'shuvo', 'no'),
(29, 'ma keno eto vhalo', 'shuvo', 'no'),
(30, 'baba keno eto obuj', 'shuvo', 'no'),
(32, 'ami keno eto slow', 'shuvo', 'no'),
(33, 'jani na amar ki hbe', 'shuvo', 'no'),
(34, 'swidesh', 'shuvo', 'no');

-- --------------------------------------------------------

--
-- Table structure for table `city`
--

CREATE TABLE `city` (
  `id` int(11) NOT NULL,
  `name` varchar(111) COLLATE utf8_croatian_ci NOT NULL,
  `city_name` varchar(111) COLLATE utf8_croatian_ci NOT NULL,
  `soft_delete` varchar(11) COLLATE utf8_croatian_ci NOT NULL DEFAULT 'no'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_croatian_ci;

--
-- Dumping data for table `city`
--

INSERT INTO `city` (`id`, `name`, `city_name`, `soft_delete`) VALUES
(2, 'rubel', 'Dhaka', 'no'),
(4, 'shuvo', 'Chittagong', 'no'),
(5, 'mow', 'Dhaka', 'no'),
(6, 'baba', 'Chittagong', 'no'),
(7, 'rahul', 'chittagong', 'no'),
(8, 'ovi', 'chittagong', 'no'),
(9, 'valluk', 'chittagong', 'no'),
(10, 'urmi', 'chittagong', 'no'),
(11, 'sir', 'chittagong', 'no'),
(12, 'BITM', 'chittagong', 'no'),
(13, 'bual', 'Dhaka', 'no'),
(14, 'paglu', 'Dhaka', 'no'),
(15, 'ali baba', 'chittagong', 'no'),
(16, 'cox', 'chittagong', 'no'),
(17, 'sohid minnar', 'Dhaka', 'no'),
(18, 'rangamati', 'chittagong', 'no'),
(19, 'rubel', 'Dhaka', 'no');

-- --------------------------------------------------------

--
-- Table structure for table `email`
--

CREATE TABLE `email` (
  `id` int(11) NOT NULL,
  `name` varchar(111) COLLATE utf8_croatian_ci NOT NULL,
  `email` varchar(111) COLLATE utf8_croatian_ci NOT NULL,
  `soft_delete` varchar(11) COLLATE utf8_croatian_ci NOT NULL DEFAULT 'no'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_croatian_ci;

--
-- Dumping data for table `email`
--

INSERT INTO `email` (`id`, `name`, `email`, `soft_delete`) VALUES
(1, 'shuvo', 'shuvomohajan@gmail.com', 'no'),
(2, 'shuvo', 'shuvomohajan97@gmail.com', 'no'),
(3, 'shuvo', 'p1r4t3_k1ng@yahoo.com', 'no'),
(4, 'oshok', 'oshok@gmail.com', 'no'),
(6, 'oshok', 'oshok@gmail.com', 'no'),
(8, 'oshok', 'oshok@gmail.com', 'no'),
(9, 'santu', 'santu@gmail.com', 'no'),
(10, 'oshok', 'oshok@gmail.com', 'no'),
(11, 'santu', 'santu@gmail.com', 'no'),
(12, 'oshok', 'oshok@gmail.com', 'no'),
(13, 'santu', 'santu@gmail.com', 'no'),
(14, 'oshok', 'oshok@gmail.com', 'no'),
(15, 'santu', 'santu@gmail.com', 'no'),
(16, 'oshok', 'oshok@gmail.com', 'no'),
(17, 'santu', 'santu@gmail.com', 'no'),
(18, 'oshok', 'oshok@gmail.com', 'no'),
(19, 'santu', 'santu@gmail.com', 'no'),
(20, 'oshok', 'oshok@gmail.com', 'no'),
(22, 'oshok', 'oshok@gmail.com', 'no'),
(23, 'santu', 'santu@gmail.com', 'no'),
(24, 'oshok', 'oshok@gmail.com', 'no'),
(25, 'santu', 'santu@gmail.com', 'no'),
(26, 'oshok', 'oshok@gmail.com', 'no'),
(27, 'santu', 'santu@gmail.com', 'no'),
(28, 'oshok', 'oshok@gmail.com', 'no'),
(29, 'santu', 'santu@gmail.com', 'no'),
(30, 'oshok', 'oshok@gmail.com', 'no'),
(31, 'santu', 'santu@gmail.com', 'no'),
(32, 'shuvo', 'shuvo@gmail.com', 'no'),
(33, 'sumon', 'sumon@gmail.com', 'no'),
(34, 'sumon', 'sumon@gmail.com', 'no'),
(35, 'sumon', 'sumon@gmail.com', 'no'),
(36, 'sumon', 'sumon@gmail.com', 'no'),
(37, 'bala', 'bala@gmail.com', 'no'),
(38, 'sumon', 'sumon@gmail.com', 'no'),
(39, 'lava', 'lava@gmail.com', 'no');

-- --------------------------------------------------------

--
-- Table structure for table `gender`
--

CREATE TABLE `gender` (
  `id` int(11) NOT NULL,
  `name` varchar(111) COLLATE utf8_croatian_ci NOT NULL,
  `gender` varchar(111) COLLATE utf8_croatian_ci NOT NULL,
  `soft_delete` varchar(11) COLLATE utf8_croatian_ci NOT NULL DEFAULT 'no'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_croatian_ci;

--
-- Dumping data for table `gender`
--

INSERT INTO `gender` (`id`, `name`, `gender`, `soft_delete`) VALUES
(1, 'shuvo', 'male', 'no'),
(2, 'shaly', 'female', 'no'),
(3, 'apu', 'female', 'no'),
(4, 'nonoy', 'male', 'no'),
(5, 'noksi', 'female', 'no'),
(6, 'nonoy', 'male', 'no'),
(7, 'noksi', 'female', 'no'),
(8, 'noksi', 'female', 'no'),
(9, 'noksi', 'female', 'no'),
(10, 'noksi', 'female', 'no'),
(11, 'noksi', 'female', 'no'),
(12, 'noksi', 'female', 'no'),
(13, 'noksi', 'female', 'no'),
(14, 'noksi', 'female', 'no'),
(15, 'noksi', 'female', 'no'),
(16, 'babu', 'male', 'no'),
(17, 'babu', 'male', 'no'),
(18, 'babu', 'male', 'no'),
(19, 'babu', 'male', 'no'),
(20, 'babu', 'male', 'no'),
(21, 'babu', 'male', 'no'),
(22, 'babu', 'male', 'no'),
(23, 'babu', 'male', 'no'),
(24, 'babu', 'male', 'no'),
(25, 'babu', 'male', 'no'),
(26, 'babu', 'male', 'no'),
(27, 'babu', 'male', 'no'),
(28, 'babu', 'male', 'no'),
(29, 'babu', 'male', 'no'),
(30, 'babu', 'male', 'no');

-- --------------------------------------------------------

--
-- Table structure for table `hobbies`
--

CREATE TABLE `hobbies` (
  `id` int(11) NOT NULL,
  `name` varchar(111) COLLATE utf8_croatian_ci NOT NULL,
  `hobbies` varchar(111) COLLATE utf8_croatian_ci NOT NULL,
  `soft_delete` varchar(11) COLLATE utf8_croatian_ci NOT NULL DEFAULT 'no'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_croatian_ci;

--
-- Dumping data for table `hobbies`
--

INSERT INTO `hobbies` (`id`, `name`, `hobbies`, `soft_delete`) VALUES
(1, 'shuvo', 'Programming,Drawing', 'no'),
(2, 'apu', 'Facebooking', 'no'),
(4, 'oshok', 'Programming', 'no'),
(5, 'santu', 'Drawing,Facebooking', 'no'),
(6, 'oshok', 'Programming', 'no'),
(7, 'oshok', 'Programming', 'no'),
(47, 'shuvo', 'Programming,Drawing', 'no'),
(48, 'shuvo', 'Programming,Drawing,Facebooking', 'no'),
(49, 'oshok', 'Facebooking', 'no'),
(50, 'shuvo', 'Programming,Facebooking', 'no'),
(51, 'oshok', 'Facebooking', 'no'),
(52, 'shuvo', 'Programming,Facebooking', 'no'),
(53, 'oshok', 'Facebooking', 'no'),
(54, 'shuvo', 'Programming,Facebooking', 'no'),
(55, 'oshok', 'Facebooking', 'no'),
(56, 'shuvo', 'Programming,Facebooking', 'no'),
(57, 'oshok', 'Facebooking', 'no'),
(58, 'shuvo', 'Programming,Facebooking', 'no'),
(59, 'oshok', 'Facebooking', 'no'),
(60, 'shuvo', 'Programming,Facebooking', 'no'),
(61, 'oshok', 'Facebooking', 'no'),
(62, 'shuvo', 'Programming,Facebooking', 'no'),
(63, 'oshok', 'Facebooking', 'no'),
(64, 'shuvo', 'Programming,Facebooking', 'no'),
(65, 'oshok', 'Facebooking', 'no'),
(66, 'shuvo', 'Programming,Facebooking', 'no'),
(67, 'oshok', 'Facebooking', 'no'),
(68, 'shuvo', 'Programming,Facebooking', 'no'),
(69, 'oshok', 'Facebooking', 'no'),
(70, 'shuvo', 'Programming,Facebooking', 'no'),
(71, 'oshok', 'Facebooking', 'no');

-- --------------------------------------------------------

--
-- Table structure for table `profile_picture`
--

CREATE TABLE `profile_picture` (
  `id` int(11) NOT NULL,
  `name` varchar(111) COLLATE utf8_croatian_ci NOT NULL,
  `pic_name` varchar(111) COLLATE utf8_croatian_ci NOT NULL,
  `pic` varchar(11111) COLLATE utf8_croatian_ci NOT NULL,
  `soft_delete` varchar(11) COLLATE utf8_croatian_ci NOT NULL DEFAULT 'no'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_croatian_ci;

--
-- Dumping data for table `profile_picture`
--

INSERT INTO `profile_picture` (`id`, `name`, `pic_name`, `pic`, `soft_delete`) VALUES
(1, 'shuvo', '209164.jpg', 'C:\\xampp\\tmp\\phpC279.tmp', 'no'),
(3, 'shuvo', '1820836_3.jpg', 'C:\\xampp\\tmp\\php7F93.tmp', 'no'),
(4, 'programmer', '209164.jpg', 'C:\\xampp\\tmp\\php345C.tmp', 'no'),
(5, 'shuvo', '1525108.jpg', 'C:\\xampp\\tmp\\php3D51.tmp', 'no'),
(6, 'ulala', 'Gir CG 1920.JPG', 'C:\\xampp\\tmp\\phpD905.tmp', 'no'),
(9, 'cartoon', '1644675.jpg', 'C:\\xampp\\tmp\\php9F44.tmp', 'no'),
(13, 'dragon', 'GW Dragon Blade.jpg', 'C:\\xampp\\tmp\\phpD766.tmp', 'no'),
(14, 'headphone', '1884176.jpg', 'C:\\xampp\\tmp\\php4DE2.tmp', 'no'),
(15, 'cocacola', 'cocacola.jpg', 'C:\\xampp\\tmp\\phpA7AB.tmp', 'no'),
(16, 'circute', 'circuit.jpg', 'C:\\xampp\\tmp\\php1FBB.tmp', 'no'),
(17, 'bilai', 'wallpapers-36.jpg', 'C:\\xampp\\tmp\\php9F4D.tmp', 'no'),
(18, 'hacker', 'hackers.jpg', 'C:\\xampp\\tmp\\phpD840.tmp', 'no'),
(19, '3-idiots', 'i-quit.png', 'C:\\xampp\\tmp\\php75B.tmp', 'no'),
(20, 'super man', 'KUaJCVr.jpg', 'C:\\xampp\\tmp\\php7C29.tmp', 'no');

-- --------------------------------------------------------

--
-- Table structure for table `summary_of_organization`
--

CREATE TABLE `summary_of_organization` (
  `id` int(11) NOT NULL,
  `name` varchar(111) COLLATE utf8_croatian_ci NOT NULL,
  `describes` varchar(111) COLLATE utf8_croatian_ci NOT NULL,
  `soft_delete` varchar(11) COLLATE utf8_croatian_ci NOT NULL DEFAULT 'no'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_croatian_ci;

--
-- Dumping data for table `summary_of_organization`
--

INSERT INTO `summary_of_organization` (`id`, `name`, `describes`, `soft_delete`) VALUES
(1, 'shuvo', 'i m shuvo', 'no'),
(2, 'shuvo', 'i \r\nam \r\nprogrammer', 'no'),
(3, 'shuvo', 'i<br />\r\nam<br />\r\na <br />\r\nbangladeshi', 'no'),
(4, 'shuvo', 'hi', 'no'),
(5, 'shuvo', 'ola madrid', 'no'),
(6, 'shuvo', 'hgcjhgjhg,kbjkjblknddksdfsf', 'no');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `birth_day`
--
ALTER TABLE `birth_day`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `book_title`
--
ALTER TABLE `book_title`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `city`
--
ALTER TABLE `city`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `email`
--
ALTER TABLE `email`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `gender`
--
ALTER TABLE `gender`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `hobbies`
--
ALTER TABLE `hobbies`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `profile_picture`
--
ALTER TABLE `profile_picture`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `summary_of_organization`
--
ALTER TABLE `summary_of_organization`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `birth_day`
--
ALTER TABLE `birth_day`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=31;
--
-- AUTO_INCREMENT for table `book_title`
--
ALTER TABLE `book_title`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=35;
--
-- AUTO_INCREMENT for table `city`
--
ALTER TABLE `city`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=20;
--
-- AUTO_INCREMENT for table `email`
--
ALTER TABLE `email`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=40;
--
-- AUTO_INCREMENT for table `gender`
--
ALTER TABLE `gender`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=33;
--
-- AUTO_INCREMENT for table `hobbies`
--
ALTER TABLE `hobbies`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=72;
--
-- AUTO_INCREMENT for table `profile_picture`
--
ALTER TABLE `profile_picture`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=21;
--
-- AUTO_INCREMENT for table `summary_of_organization`
--
ALTER TABLE `summary_of_organization`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
