<?php
    namespace App\BirthDay;

    use App\Message\Message;
    use App\Model\Database as DB;
    use App\Utility\Utility;
    use PDO;

    class BirthDay extends DB
    {
        private $id;
        private $name;
        private $birth_day;

        public function setData($allPostData=null)
        {
            if(array_key_exists("id",$allPostData)){
                $this->id = $allPostData['id'];
            }
            if(array_key_exists("name",$allPostData)){
                $this->name = $allPostData['name'];
            }
            if(array_key_exists("dob",$allPostData)){
                $this->birth_day = $allPostData['dob'];
            }
        }

        public function store(){
            $arraData = array($this->name, $this->birth_day);
            $query = "insert into birth_day(name, birth_day) VALUES (?,?)";
            $STH = $this->DBH->prepare($query);
            $result = $STH->execute($arraData);
            if($result){
                Message::setMessage("Success! Data has been stored Successfully...");
            }else{
                Message::setMessage("Failed! Data has not been stored Successfully...");
            }
            Utility::redirect("index.php");
        }

        public function index(){
            $sql = "select *from birth_day where soft_delete='no'";
            $sth = $this->DBH->query($sql);
            $sth->setFetchMode(PDO::FETCH_OBJ);
            return $sth->fetchAll();
        }
        public function view(){
            $sql = "select * from birth_day where id=".$this->id;
            $sth = $this->DBH->query($sql);
            $sth->setFetchMode(PDO::FETCH_OBJ);
            return $sth->fetch();
        }
        public function update(){
            $arraData = array($this->name,$this->birth_day);
            $query = 'UPDATE birth_day SET name=?, birth_day=? WHERE id='.$this->id;
            $sth = $this->DBH->prepare($query);
            $result = $sth->execute($arraData);
            if($result){
                Message::setMessage("Success! Data has been updated Successfully...");
            }else{
                Message::setMessage("Failed! Data has not been updated Successfully...");
            }
            Utility::redirect("index.php");
        }

        public function trashed(){
            $sql = "select *from birth_day where soft_delete='yes'";
            $sth = $this->DBH->query($sql);
            $sth->setFetchMode(PDO::FETCH_OBJ);
            return $sth->fetchAll();
        }
        public function trash(){
            $arraData = array("yes");
            $query = 'UPDATE birth_day SET soft_delete=? WHERE id='.$this->id;
            $sth = $this->DBH->prepare($query);
            $result = $sth->execute($arraData);
            if($result){
                Message::setMessage("Success! Data has been trashed Successfully...");
            }else{
                Message::setMessage("Failed! Data has not been trashed Successfully...");
            }
            Utility::redirect("trashed.php");
        }
        public function recover(){
            $arraData = array("no");
            $query = 'UPDATE birth_day SET soft_delete=? WHERE id='.$this->id;
            $sth = $this->DBH->prepare($query);
            $result = $sth->execute($arraData);
            if($result){
                Message::setMessage("Success! Data has been recovered Successfully...");
            }else{
                Message::setMessage("Failed! Data has not been recovered Successfully...");
            }
            Utility::redirect("index.php");
        }
        public function delete(){
            $sql = "DELETE FROM birth_day WHERE id=".$this->id;
            $result = $this->DBH->exec($sql);
            if($result){
                Message::setMessage("Success! Data has been deleted Successfully...");
            }else{
                Message::setMessage("Failed! Data has not been deleted Successfully...");
            }
            Utility::redirect("index.php");
        }
        public function indexPaginator($page=1,$itemsPerPage=5){

            $start = (($page-1) * $itemsPerPage);
            $sql = "SELECT * from birth_day  WHERE soft_delete = 'No' LIMIT $start,$itemsPerPage";
            $STH = $this->DBH->query($sql);
            $STH->setFetchMode(PDO::FETCH_OBJ);
            return $STH->fetchAll();
        }
        public function trashedPaginator($page=1,$itemsPerPage=15){

            $start = (($page-1) * $itemsPerPage);
            $sql = "SELECT * from birth_day  WHERE soft_delete = 'yes' LIMIT $start,$itemsPerPage";
            $STH = $this->DBH->query($sql);
            $STH->setFetchMode(PDO::FETCH_OBJ);
            return $STH->fetchAll();
        }
        public function search($requestArray){
            $sql = "";
            if( $requestArray['select'] == "byBoth")  $sql = "SELECT * FROM `birth_day` WHERE `soft_delete` ='No' AND (`name` LIKE '%".$requestArray['search']."%' OR `birth_day` LIKE '%".$requestArray['search']."%')";
            if( $requestArray['select'] == "byName" ) $sql = "SELECT * FROM `birth_day` WHERE `soft_delete` ='No' AND `name` LIKE '%".$requestArray['search']."%'";
            if( $requestArray['select'] == "byBirthDay" )  $sql = "SELECT * FROM `birth_day` WHERE `soft_delete` ='No' AND `birth_day` LIKE '%".$requestArray['search']."%'";

            $STH  = $this->DBH->query($sql);
            $STH->setFetchMode(PDO::FETCH_OBJ);
            $someData = $STH->fetchAll();
            return $someData;
        }
        public function getAllKeywords()
        {
            $_allKeywords = array();
            $WordsArr = array();

            // for each search field block for birth day start
            $allData = $this->index();

            foreach ($allData as $oneData) {
                $_allKeywords[] = trim($oneData->name);
            }

            $allData = $this->index();

            foreach ($allData as $oneData) {

                $eachString= strip_tags($oneData->name);
                $eachString=trim( $eachString);
                $eachString= preg_replace( "/\r|\n/", " ", $eachString);
                $eachString= str_replace("&nbsp;","",  $eachString);
                $WordsArr = explode(" ", $eachString);

                foreach ($WordsArr as $eachWord){
                    $_allKeywords[] = trim($eachWord);
                }
            }

            // for each search field block for birth day start
            $allData = $this->index();

            foreach ($allData as $oneData) {
                $_allKeywords[] = trim($oneData->birth_day);
            }
            $allData = $this->index();

            foreach ($allData as $oneData) {

                $eachString= strip_tags($oneData->birth_day);
                $eachString=trim( $eachString);
                $eachString= preg_replace( "/\r|\n/", " ", $eachString);
                $eachString= str_replace("&nbsp;","",  $eachString);
                $WordsArr = explode(" ", $eachString);

                foreach ($WordsArr as $eachWord){
                    $_allKeywords[] = trim($eachWord);
                }
            }
            return array_unique($_allKeywords);
        }


        public function MultipleDelete($allDeleteData)
        {
            $softDeleteData = "";
            $success = count($allDeleteData);   //For Checking Success
            $counter = 0;   //For Checking Success
            foreach($allDeleteData as $singleData){
                $this->id = $singleData;
                if($counter != 0){

                    //Detecting soft-delete value for perfect redirection
                    if($counter == 1){
                        $sql = "SELECT soft_delete FROM birth_day WHERE id = '".$this->id."'";
                        $STH = $this->DBH->query($sql);
                        $STH->setFetchMode(\PDO::FETCH_OBJ);
                        $softDeleteData = $STH->fetch();
                    }

                    $arrayData = array($this->id);
                    $query = "DELETE FROM birth_day WHERE id = ?";
                    $STH = $this->DBH->prepare($query);
                    $result = $STH->execute($arrayData);
                }
                $counter++;//Counting For Checking Success
            }

            //Redirecting on perfect page
            if(($counter == $success) && ($softDeleteData->soft_delete == "no")){
                Message::message("Success! Selected Data has been Deleted.");
                Utility::redirect('index.php?Page=1');
            }
            elseif(($counter == $success) && ($softDeleteData->soft_delete == "Yes")){
                Message::message("Success! Selected Data has been Deleted.");
                Utility::redirect('trashed.php?Page=1');
            }
            elseif(($counter != $success) && ($softDeleteData->soft_delete == "no")){
                Message::message("Failed! Selected Data has not been Deleted.");
                Utility::redirect('index.php?Page=1');
            }
            else{
                Message::message("Failed! Selected Data has not been Deleted.");
                Utility::redirect('trashed.php?Page=1');
            }
        }

        public function MultipleTrash($allDeleteData)
        {
            $success = count($allDeleteData); //For Checking Success
            $counter = 0; //For Checking Success

            //Updating soft_delete value one by one for Selected Item
            foreach($allDeleteData as $singleData){
                $this->id = $singleData;
                if($counter != 0){
                    $arrayData = array("Yes",$this->id);
                    $query = 'UPDATE birth_day SET soft_delete = ? WHERE id = ?';
                    $STH = $this->DBH->prepare($query);
                    $result = $STH->execute($arrayData);
                }
                $counter++; //Counting For Checking Success
            }

            if($counter == $success){
                Message::message("Success! Selected Data has been Deleted.");
                Utility::redirect('index.php?Page=1');
            }
            else{
                Message::message("Failed! Selected Data has not been Deleted.");
                Utility::redirect('index.php?page=1');
            }
        }

        public function MultipleRecover($allDeleteData)
        {
            $success = count($allDeleteData);   //For Checking Success
            $counter = 0;   //For Checking Success
            foreach($allDeleteData as $singleData){
                $this->id = $singleData;
                if($counter != 0){
                    $arrayData = array("no",$this->id);
                    $query = 'UPDATE birth_day SET soft_delete = ? WHERE id = ?';
                    $STH = $this->DBH->prepare($query);
                    $result = $STH->execute($arrayData);
                }
                $counter++; //Counting For Checking Success
            }

            if($counter == $success){
                Message::message("Success! Selected Data has been Recovered.");
                Utility::redirect('trashed.php?Page=1');
            }
            else{
                Message::message("Failed! Selected Data has not been Recoverd.");
                Utility::redirect('trashed.php?page=1');
            }
        }
    }
?>