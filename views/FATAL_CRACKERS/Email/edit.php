<?php
require_once ("../../../vendor/autoload.php");
$objEmail = new \App\Email\Email();
$objEmail->setData($_GET);
$oneData = $objEmail->view();
?>

<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Email</title>
    <link rel="stylesheet" href="../../../resource/bootstrap/css/bootstrap.min.css">
    <script src="../../../resource/bootstrap/js/bootstrap.min.js"></script>
    <link rel="stylesheet" href="../../../resource/css/style.css">
</head>
<body>
    <div class="container">
        <div id="InputForm" class="col-md-8 col-md-offset-2">
            <form action="update.php" method="post" class="form">
                <label>Enter Your Name &nbsp;: </label>
                <input type="text" name="name"  value="<?php echo $oneData->name ?>" required><br><br>
                <label>Enter Your Email : </label>
                <input type="email" name="email"  value="<?php echo $oneData->email ?>" required><br><br>
                <input type="hidden" name="id" value="<?php echo $oneData->id ?>">
                <input type="submit">
            </form>
        </div>
    </div>
</body>
</html>