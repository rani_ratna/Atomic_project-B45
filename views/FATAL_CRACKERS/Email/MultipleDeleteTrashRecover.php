<?php
require_once("../../../vendor/autoload.php");

$objEmail = new \App\Email\Email();

if(($_POST['btn'] == "TrashAll")){
    $objEmail->MultipleTrash($_POST);
} elseif(($_POST['btn'] == "DeleteAll")){
    $objEmail->MultipleDelete($_POST);
}elseif(($_POST['btn'] == "RecoverAll")){
    $objEmail->MultipleRecover($_POST);
}