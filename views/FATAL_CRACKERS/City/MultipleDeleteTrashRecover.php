<?php
require_once("../../../vendor/autoload.php");

$objCity = new \App\City\City();

if(($_POST['btn'] == "TrashAll")){
    $objCity->MultipleTrash($_POST);
} elseif(($_POST['btn'] == "DeleteAll")){
    $objCity->MultipleDelete($_POST);
}elseif(($_POST['btn'] == "RecoverAll")){
    $objCity->MultipleRecover($_POST);
}