<?php
require_once ("../../../vendor/autoload.php");
$objCity = new \App\City\City();
$objCity->setData($_GET);
$oneData = $objCity->view();
?>
<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>City</title>
    <link rel="stylesheet" href="../../../resource/bootstrap/css/bootstrap.min.css">
    <script src="../../../resource/bootstrap/js/bootstrap.min.js"></script>
    <link rel="stylesheet" href="../../../resource/css/style.css">
</head>
<body>
    <div class="container">
        <div id="InputForm" class="col-md-8 col-md-offset-2">
            <form action="update.php" method="post" class="form">
                <label>Enter Your Name &nbsp;&nbsp;&nbsp;: </label>
                <input type="text" name="name" placeholder="  your name" value="<?php echo $oneData->name ?>" required><br><br>
                <labeL>Enter Your City &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;: </labeL>
                <select name="city">
                    <option <?php if($oneData->city_name=="Chittagong")echo "selected"?> >Chittagong</option>
                    <option <?php if($oneData->city_name=="Dhaka")echo "selected"?> >Dhaka</option>
                    <option <?php if($oneData->city_name=="Rangpur")echo "selected"?> >Rangpur</option>
                    <option <?php if($oneData->city_name=="Mymensingh")echo "selected"?> >Mymensingh</option>
                    <option <?php if($oneData->city_name=="Sylhet")echo "selected"?> >Sylhet</option>
                    <option <?php if($oneData->city_name=="Khulna")echo "selected"?> >Khulna</option>
                    <option <?php if($oneData->city_name=="Barisal")echo "selected"?> >Barisal</option>
                    <option <?php if($oneData->city_name=="Rajshahi")echo "selected"?> >Rajshahi</option>
                </select><br><br>
                <input type="hidden" name="id" value="<?php echo $oneData->id ?>">
                <input type="submit" value="Update">
            </form>
        </div>
    </div>
</body>
</html>