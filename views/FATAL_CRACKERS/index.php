<html>
<head>
    <link rel="stylesheet" href="../../resource/bootstrap/css/bootstrap.min.css">
    <script src="../../resource/bootstrap/js/bootstrap.min.js"></script>
    <script src="../../resource/js/jquery-3.1.1.min.js"></script>
    <link rel="stylesheet" href="../../resource/css/style.css">
    <style>
        #box{
            margin: auto 120px;
            padding: 10px;
            position: relative;
            background: rgba(0,0,0,0.5);
            border-radius: 8px;
            overflow: hidden;
        }
        #box1{
            border-radius: 100%;
            height: 150px;
            width: 150px;
            border: 1px solid #222;
            margin:200px auto;
            position: relative;
            z-index: 111;
            background:radial-gradient(rgba(0,0,0,0.2),black);
            box-shadow: 0 0 20px 5px dodgerblue;
            transition: all 0.3s ease-in 0s;
        }
        #box1:hover{
            -webkit-transform: scale(1.1);
            transform: scale(1.1);
            box-shadow: 0 0 20px 5px dodgerblue;
        }
        #box1:active{
            box-shadow:inset 4px 4px  5px black;
        }
        #box1>h5{
            line-height: 130px;
            text-align: center;
            color: white;
        }
        div>a{
            display: block;
            color: white;
            text-align: center;
            line-height: 100px;
        }
        div>a:hover{
            text-decoration: none;
            color: white;
        }
        #box>div:nth-child(2):hover,#box>div:nth-child(3):hover,#box>div:nth-child(4):hover,#box>div:nth-child(5):hover,#box>div:nth-child(6):hover,#box>div:nth-child(7):hover,#box>div:nth-child(8):hover,#box>div:nth-child(9):hover{
        }
        #elastic{
            border-radius: 100%;
            height: 100px;
            width: 100px;
            background: #337AB7;
            animation: elastic 1s;
            position:absolute;
            left: 410px;
            top: 245px;
            transition: all 0.3s ease-in-out 0s;
            opacity: 0;
            box-shadow: 0 0 20px 2px black;
            overflow: hidden;
        }#elastic>span{
            display: block;
            height: 100px;
            width: 100px;
            border-radius: 100%;
            background: url(../../resource/images/birthday-cake.png) no-repeat scroll 30px 20px;
            background-size: 40px;
        }
        #elastic1{
            border-radius: 100%;
            height: 100px;
            width: 100px;
            background: #337AB7;
            animation: elastic 1s;
            position:absolute;
            left: 410px;
            top: 245px;
            transition: all 0.4s ease-in-out 0s;
            opacity: 0;
            box-shadow: 0 0 15px 0 black;
            overflow: hidden;
        }#elastic1>span{
            display: block;
            height: 100px;
            width: 100px;
            border-radius: 100%;
            background: url(../../resource/images/bookshelf.png) no-repeat scroll 30px 30px;
            background-size: 40px;
        }
        #elastic2{
            border-radius: 100%;
            height: 100px;
            width: 100px;
            background:#337AB7;
            animation: elastic 1s;
            position:absolute;
            left: 410px;
            top: 245px;
            transition: all 0.5s ease-in-out 0s;
            opacity: 0;
            box-shadow: 0 0 15px 0 black;
            overflow: hidden;
        }#elastic2>span{
            display: block;
            height: 100px;
            width: 100px;
            border-radius: 100%;
            background: url(../../resource/images/skyscrapper.png) no-repeat scroll 30px 25px;
            background-size: 40px;
        }
        #elastic3{
            border-radius: 100%;
            height: 100px;
            width: 100px;
            background: #337AB7;
            animation: elastic 1s;
            position:absolute;
            left: 410px;
            top: 245px;
            transition: all 0.6s ease-in-out 0s;
            opacity: 0;
            box-shadow: 0 0 15px 0 black;
            overflow: hidden;
        }#elastic3>span{
            display: block;
            height: 100px;
            width: 100px;
            border-radius: 100%;
            background: url(../../resource/images/email.png) no-repeat scroll 30px 30px;
            background-size: 40px;
        }
        #elastic4{
            border-radius: 100%;
            height: 100px;
            width: 100px;
            background: #337AB7;
            animation: elastic 1s;
            position:absolute;
            left: 410px;
            top: 245px;
            transition: all 0.7s ease-in-out 0s;
            opacity: 0;
            box-shadow: 0 0 15px 0 black;
            overflow: hidden;
        }#elastic4>span{
            display: block;
            height: 100px;
            width: 100px;
            border-radius: 100%;
            background: url(../../resource/images/gender.png) no-repeat scroll 30px 30px;
            background-size: 40px;
        }
        #elastic5{
            border-radius: 100%;
            height: 100px;
            width: 100px;
            background: #337AB7;
            animation: elastic 1s;
            position:absolute;
            left: 410px;
            top: 245px;
            transition: all 0.8s ease-in-out 0s;
            opacity: 0;
            box-shadow: 0 0 15px 0 black;
            overflow: hidden;
        }#elastic5>span{
            display: block;
            height: 100px;
            width: 100px;
            border-radius: 100%;
            background: url(../../resource/images/bricks.png) no-repeat scroll 30px 30px;
            background-size: 40px;
        }
        #elastic6{
            border-radius: 100%;
            height: 100px;
            width: 100px;
            background: #337AB7;
            animation: elastic 1s;
            position:absolute;
            left: 410px;
            top: 245px;
            transition: all 0.9s ease-in-out 0s;
            opacity: 0;
            box-shadow: 0 0 15px 0 black;
            overflow: hidden;
        }#elastic6>span{
            display: block;
            height: 100px;
            width: 100px;
            border-radius: 100%;
            background: url(../../resource/images/image.png) no-repeat scroll 30px 30px;
            background-size: 40px;
        }
        #elastic7{
            border-radius: 100%;
            height: 100px;
            width: 100px;
            background: #337AB7;
            animation: elastic 1s;
            position:absolute;
            left: 410px;
            top: 245px;
            transition: all 1s ease-in-out 0s;
            opacity: 0;
            box-shadow: 0 0 15px 0 black;
            overflow: hidden;
        }#elastic7>span{
            display: block;
            height: 100px;
            width: 100px;
            border-radius: 100%;
            background: url(../../resource/images/notepad.png) no-repeat scroll 30px 30px;
            background-size: 40px;
        }
    </style>
    <script>
        count=1;

        function show(){
            count++;
                if(count%2==0){
                ela1 = document.getElementById('elastic');
                ela2 = document.getElementById('elastic1');
                ela3 = document.getElementById('elastic2');
                ela4 = document.getElementById('elastic3');
                ela5 = document.getElementById('elastic4');
                ela6 = document.getElementById('elastic5');
                ela7 = document.getElementById('elastic6');
                ela8 = document.getElementById('elastic7');

                ela1.style.top="72px";
                ela1.style.left="400px";
                    
                ela2.style.top="120px";
                ela2.style.left="515px";
                    
                ela3.style.top="230px";
                ela3.style.left="565px";
                    
                ela4.style.top="345px";
                ela4.style.left="515px";
                    
                ela5.style.top="395px";
                ela5.style.left="400px";
                    
                ela6.style.top="345px";
                ela6.style.left="285px";
                    
                ela7.style.top="230px";
                ela7.style.left="230px";
                    
                ela8.style.top="120px";
                ela8.style.left="285px";
                ela1.style.opacity="1";
                ela2.style.opacity="1";
                ela3.style.opacity="1";
                ela4.style.opacity="1";
                ela5.style.opacity="1";
                ela6.style.opacity="1";
                ela7.style.opacity="1";
                ela8.style.opacity="1";
            }else{
                    ela1 = document.getElementById('elastic');
                    ela2 = document.getElementById('elastic1');
                    ela3 = document.getElementById('elastic2');
                    ela4 = document.getElementById('elastic3');
                    ela5 = document.getElementById('elastic4');
                    ela6 = document.getElementById('elastic5');
                    ela7 = document.getElementById('elastic6');
                    ela8 = document.getElementById('elastic7');

                    ela1.style.top="";
                    ela1.style.left="";
                    ela2.style.top="";
                    ela2.style.left="";
                    ela3.style.top="";
                    ela3.style.left="";
                    ela4.style.top="";
                    ela4.style.left="";
                    ela5.style.top="";
                    ela5.style.left="";
                    ela6.style.top="";
                    ela6.style.left="";
                    ela7.style.top="";
                    ela7.style.left="";
                    ela8.style.top="";
                    ela8.style.left="";
                    ela1.style.opacity="";
                    ela2.style.opacity="";
                    ela3.style.opacity="";
                    ela4.style.opacity="";
                    ela5.style.opacity="";
                    ela6.style.opacity="";
                    ela7.style.opacity="";
                    ela8.style.opacity="";
                }
        }
    </script>
</head>
    <body>
        <div class="container">
            <div id="box">
                <div id="box1" onclick="show()"><h5>ATOMIC PROJECT</h5></div>
                <a href="BirthDay/index.php"><div id="elastic"><span></span></div></a>
                <a href="BookTitle/index.php"><div id="elastic1"><span></span></div></a>
                <a href="City/index.php"><div id="elastic2"><span></span></div></a>
                <a href="Email/index.php"><div id="elastic3"><span></span></div></a>
                <a href="Gender/index.php"><div id="elastic4"><span></span></div></a>
                <a href="Hobbies/index.php"><div id="elastic5"><span></span></div></a>
                <a href="ProfilePicture/index.php"><div id="elastic6"><span></span></div></a>
                <a href="SummaryOfOrganization/index.php"><div id="elastic7"><span></span></div></a>
            </div>
        </div>
    </body>
</html>