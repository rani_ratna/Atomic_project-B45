<?php
require_once ("../../../vendor/autoload.php");
$objGender = new \App\Gender\Gender();
$objGender->setData($_GET);
$oneData = $objGender->view();
?>

<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Gender</title>
    <link rel="stylesheet" href="../../../resource/bootstrap/css/bootstrap.min.css">
    <script src="../../../resource/bootstrap/js/bootstrap.min.js"></script>
    <link rel="stylesheet" href="../../../resource/css/style.css">
</head>
<body>
    <div class="container">
        <div id="InputForm" class="col-md-8 col-md-offset-2">
            <form action="update.php" method="post" class="form">
                <label>Enter Your Name : </label>
                <input type="text" name="name" value="<?php echo $oneData->name ?>" required><br><br>
                <input type="radio" name="gender" value="male" <?php if($oneData->gender=="male")echo "checked"?> > male
                &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                <input type="radio" name="gender" value="female" <?php if($oneData->gender=="female")echo "checked"?> > female<br><br>
                <input type="hidden" name="id" value="<?php echo $oneData->id ?>">
                <input type="submit">
            </form>
        </div>
    </div>
</body>
</html>