<?php
require_once ("../../../vendor/autoload.php");
use App\Utility\Utility;
session_start();
$objGender = new \App\Gender\Gender();
$allData = $objGender->trashed();
/* ############################# pagination code ######################################*/
$recordCount= count($allData);
if($recordCount>0) {
    if (isset($_REQUEST['Page'])) $page = $_REQUEST['Page'];
    else if (isset($_SESSION['Page'])) $page = $_SESSION['Page'];
    else $page = 1;
    $_SESSION['Page'] = $page;

    if (isset($_REQUEST['ItemsPerPage'])) $itemsPerPage = $_REQUEST['ItemsPerPage'];
    else if (isset($_SESSION['ItemsPerPage'])) $itemsPerPage = $_SESSION['ItemsPerPage'];
    else $itemsPerPage = 10;
    $_SESSION['ItemsPerPage'] = $itemsPerPage;

    $pages = ceil($recordCount / $itemsPerPage);
    $someData = $objGender->trashedPaginator($page, $itemsPerPage);
    $serial = (($page - 1) * $itemsPerPage) + 1;
}
/*############################# pagination code ######################################*/
?>
<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Birth Day - Active List</title>
    <link rel="stylesheet" href="../../../resource/bootstrap/css/bootstrap.min.css">
    <link rel="stylesheet" href="../../../resource/jquery-ui/jquery-ui.css">
    <script src="../../../resource/js/jquery-3.1.1.min.js"></script>
    <script src="../../../resource/jquery-ui/jquery-ui.js"></script>
    <script src="../../../resource/js/scrollEvent.js"></script>
    <script src="../../../resource/bootstrap/js/bootstrap.min.js"></script>
    <link rel="stylesheet" href="../../../resource/css/style.css">
</head>
<body>
    <div class="container">
        <?php
            if(!isset($_SESSION)) session_start();
            $msg = \App\Message\Message::getMessage();

            if($msg=="Success! Data has been trashed Successfully..."){
                echo "<div id='success' class='message'><h3>$msg</h3></div>";
            }elseif($msg=="Failed! Data has not been trashed Successfully..."){
                echo "<div id='error' class='message'><h3>$msg</h3></div>";
            }
        ?>
        <a href="pdf1.php"><img src="../../../resource/images/pdf.png" alt="pdf" class="heading"></a>
        <a href="#"><img src="../../../resource/images/xls.png" alt="xls" class="heading"></a>
        <a href="#"><img src="../../../resource/images/gmail.png" alt="gmail" class="heading"></a>
        <h2 class="text-center tophead center-block" id="topHead">Book Title - Trashed List</h2>
        <div class="col-md-3" id="nav">
            <ul class="nav nav-pills nav-stacked">
                <li><a href="../BirthDay/index.php">Birth Day</a></li>
                <li><a href="../BookTitle/index.php">Book Title</a></li>
                <li><a href="../City/index.php">City</a></li>
                <li><a href="../Email/index.php">Email</a></li>
                <li class="active"><a href="../Gender/index.php">Gender</a></li>
                <li><a href="../Hobbies/index.php">Hobbies</a></li>
                <li><a href="../ProfilePicture/index.php">Profile Picture</a></li>
                <li><a href="../SummaryOfOrganization/index.php">Summary Of Organization</a></li>
            </ul><br>

            <!--##### Form for Selected Item Data Send #####-->
            <form action="MultipleDeleteTrashRecover.php" method="post">

                <button type='button' class="btn btn-danger btn-block btn-sm hide showBtn" data-toggle='modal' data-target='.targetOne'>Delete Selected</button>
                <button name="btn" value="RecoverAll" class="btn btn-warning btn-block btn-sm hide showBtn">Recover Selected</button>
                <a href="create.php" class="btn btn-primary btn-block primary btn-sm hideBtn">ADD DATA</a>

        </div>
        <div class='modal fade targetOne' tabindex ='-1' role = 'dialog' aria-labelledby = 'mySmallModalLabel' >
            <div class='modal-dialog modal-sm' role = 'document' >
                <div class='modal-content' >
                    <div class='modal-body' >
                        <button type = 'button' class='close' data-dismiss = 'modal' >&times;</button >
                        <h4 class='modal-title' > Are You Sure ?...</h4 >
                    </div >
                    <div class='modal-footer' >
                        <button name="btn" value="DeleteAll" class='btn btn-danger btn-sm'>Delete</button>
                        <button type = 'button' class='btn btn-primary' data-dismiss = 'modal' > No</button >
                    </div >
                </div >
            </div >
        </div >

        <div class="col-md-9 right" id="list">
            <table class="table table-striped table-bordered text-center" cellspacing="0">
                <thead>
                    <th><input type='checkbox' id="selectAll"> All</th>
                    <th>Serial Num</th>
                    <th>ID</th>
                    <th>Name</th>
                    <th>Gender</th>
                    <th>Action</th>
                </thead>
                <?php
                $select = 1;
                if($recordCount>0) {
                    foreach ($someData as $oneData) {
                        echo "
                            <tr>
                                <td><input type='checkbox' class='checkbox' name='check$select' value='$oneData->id'></td>
                                <td>$serial</td>
                                <td>$oneData->id</td>
                                <td>$oneData->name</td>
                                <td>$oneData->gender</td>
                                <td>
                                <a href='view.php?id=$oneData->id' class='btn btn-default btn-sm'>View</a>
                                <a href='recover.php?id=$oneData->id' class='btn btn-success btn-sm'>Recover</a>
                                <button type='button' class='btn btn-danger btn-sm' data-toggle='modal' data-target='.target$serial'>Delete</button>
                                    <div class='modal fade target$serial' tabindex ='-1' role = 'dialog' aria-labelledby = 'mySmallModalLabel' >
                                        <div class='modal-dialog modal-sm' role = 'document' >
                                            <div class='modal-content' >
                                                <div class='modal-body' >
                                                    <button type = 'button' class='close' data-dismiss = 'modal' >&times;</button >
                                                    <h4 class='modal-title' > Are You Sure ?...</h4 >
                                                </div >
                                                <div class='modal-footer' >
                                                    <a href = 'delete.php?id=$oneData->id' class='btn btn-danger' > Yes</a >
                                                    <button type = 'button' class='btn btn-primary' data-dismiss = 'modal' > No</button >
                                                </div >
                                            </div >
                                        </div >
                                    </div >
                                </td>
                            </tr>
                        ";
                        $serial++;
                        $select++;
                    }
                }
                ?>
            </table>
            </form>
            <!--##### Selected Item Data Send Form End Here #####-->


            <!--  ######################## pagination code ###################################### -->
            <?php
            if($recordCount>0) {
                echo "<div align='left'>";
                echo "<ul class='pagination'>";
                $pageMinusOne = $page - 1;
                $pagePlusOne = $page + 1;
                if ($page > $pages) Utility::redirect("trashed.php?Page=$pages");

                if ($page > 1) echo "<li><a href='trashed.php?Page=$pageMinusOne'>" . "Previous" . "</a></li>";
                for ($i = 1; $i <= $pages; $i++) {
                    if ($i == $page) echo '<li class="active"><a href="">' . $i . '</a></li>';
                    else  echo "<li><a href='?Page=$i'>" . $i . '</a></li>';
                }
                if ($page < $pages) echo "<li><a href='trashed.php?Page=$pagePlusOne'>" . "Next" . "</a></li>";

                echo "<select  class=\"form-control\"  name=\"ItemsPerPage\" id=\"ItemsPerPage\" onchange=\"javascript:location.href = this.value;\" >";
                if ($itemsPerPage == 6) echo '<option  value="?ItemsPerPage=6" selected >Show 6 Items Per Page</option>';
                else echo '<option  value="?ItemsPerPage=6">Show 6 Items Per Page</option>';

                if ($itemsPerPage == 10) echo '<option  value="?ItemsPerPage=10" selected >Show 10 Items Per Page</option>';
                else echo '<option  value="?ItemsPerPage=10">Show 10 Items Per Page</option>';

                if ($itemsPerPage == 15) echo '<option  value="?ItemsPerPage=15" selected >Show 15 Items Per Page</option>';
                else    echo '<option  value="?ItemsPerPage=15">Show 15 Items Per Page</option>';
                echo "</select>";
                echo "</ul>";
                echo "</div><br><br><br>";
            }
            ?>
            <!--  ######################## pagination code ###################################### -->
        </div>
    </div>
    <script>
        jQuery(function($){
            $('.message').fadeIn(500);
            $('.message').fadeOut(1500);
            $('.message').fadeIn(500);
            $('.message').fadeOut(1500);
            $('.message').fadeIn(500);
            $('.message').fadeOut(1500);
        });

        <!-- ################ check all When select all Start ################### -->
        $('#selectAll').change(function(){
            var status = this.checked;
            $('.checkbox').each(function(){
                this.checked = status;
            });
        });
        <!-- ################ check all When select all end ################### -->

        <!-- ################ on checked btn hide & show start ################### -->
        $('.checkbox,#selectAll').change(function(){
            if($('.checkbox').is(':checked')){
                $('.hideBtn').addClass("hide");
                $('.showBtn').removeClass("hide");
            }else{
                $('.hideBtn').removeClass("hide");
                $('.showBtn').addClass("hide");
            }
        });
        <!-- ################ on checked btn hide & show End ################### -->
    </script>
</body>
</html>