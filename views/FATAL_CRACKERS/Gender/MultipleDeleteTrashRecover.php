<?php
require_once("../../../vendor/autoload.php");

$objGender = new \App\Gender\Gender();

if(($_POST['btn'] == "TrashAll")){
    $objGender->MultipleTrash($_POST);
} elseif(($_POST['btn'] == "DeleteAll")){
    $objGender->MultipleDelete($_POST);
}elseif(($_POST['btn'] == "RecoverAll")){
    $objGender->MultipleRecover($_POST);
}