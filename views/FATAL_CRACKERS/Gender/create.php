<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Gender</title>
    <link rel="stylesheet" href="../../../resource/bootstrap/css/bootstrap.min.css">
    <script src="../../../resource/bootstrap/js/bootstrap.min.js"></script>
    <link rel="stylesheet" href="../../../resource/css/style.css">
</head>
<body>
    <div class="container">
        <div class="col-md-3">
            <ul class="nav nav-pills nav-stacked">
                <li><a href="../index.php">Home</a></li>
                <li><a href="../BirthDay/index.php">Birth Day</a></li>
                <li><a href="../BookTitle/index.php">Book Title</a></li>
                <li><a href="../City/index.php">City</a></li>
                <li><a href="../Email/index.php">Email</a></li>
                <li class="active"><a href="../Gender/index.php">Gender</a></li>
                <li><a href="../Hobbies/index.php">Hobbies</a></li>
                <li><a href="../ProfilePicture/index.php">Profile Picture</a></li>
                <li><a href="../SummaryOfOrganization/index.php">Summary Of Organization</a></li>
            </ul>
        </div>
        <div id="InputForm" class="col-md-9">
            <form action="store.php" method="post" class="form">
                <label>Enter Your Name : </label>
                <input type="text" name="name" placeholder="  your name" required><br><br>
                <input type="radio" name="gender" value="male"> male
                &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                <input type="radio" name="gender" value="female"> female<br><br>
                <input type="submit">
            </form>
        </div>
    </div>
</body>
</html>