<?php
    require_once ("../../../vendor/autoload.php");
    use App\ProfilePicture\ProfilePicture;
    $objProfilePicture = new ProfilePicture();
    $objProfilePicture->setId($_GET);
    $oneData = $objProfilePicture->view();
?>

<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Profile Picture</title>
    <link rel="stylesheet" href="../../../resource/bootstrap/css/bootstrap.min.css">
    <script src="../../../resource/bootstrap/js/bootstrap.min.js"></script>
    <script src="../../../resource/js/jquery-3.1.1.min.js"></script>
    <link rel="stylesheet" href="../../../resource/css/style.css">
</head>
<body>
    <div class="container col-md-8 col-md-offset-2">
        <h2 class="text-center tophead">Profile Picture - Active List</h2>
        <table class="table table-bordered table-striped">
            <?php
                echo "
                    <tr>
                        <th>ID :</th>
                        <td>$oneData->id</td>
                    </tr>
                    <tr>
                        <th>Name :</th>
                        <td>$oneData->name</td>
                    </tr>
                    <tr>
                        <th>Picture Name :</th>
                        <td>$oneData->pic_name</td>
                    </tr>
                    <tr>
                        <th>Picture :</th>
                        <td><img src='img/$oneData->pic_name' height='300px' width='400px'></td>
                    </tr>
                ";
            ?>
        </table>
        <a href="index.php" class="btn tophead btn-block">GO BACK</a>
    </div>
</body>
</html>
