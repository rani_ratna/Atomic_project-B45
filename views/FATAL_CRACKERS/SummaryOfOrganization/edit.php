<?php
require_once ("../../../vendor/autoload.php");
$objSummaryOfOrganization = new \App\SummaryOfOrganization\SummaryOfOrganization();
$objSummaryOfOrganization->setData($_GET);
$oneData = $objSummaryOfOrganization->view();
?>
<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Summar Of Organization</title>
    <link rel="stylesheet" href="../../../resource/bootstrap/css/bootstrap.min.css">
    <script src="../../../resource/bootstrap/js/bootstrap.min.js"></script>
    <link rel="stylesheet" href="../../../resource/css/style.css">
</head>
<body>
    <div class="container">
        <div id="InputForm" class="col-md-8 col-md-offset-2">
            <form action="update.php" method="post" class="form">
                <label>Enter Your Name &nbsp;&nbsp;&nbsp;&nbsp; : </label>
                <input type="text" name="name" value="<?php echo $oneData->name?>" required><br><br>
                <label>Enter Your Describe </label><br>
                <textarea name="describes" rows="5" cols="70" required style="color: #000;"><?php echo $oneData->describes ?></textarea><br><br>
                <input type="hidden" name="id" value="<?php echo $oneData->id ?>">
                <input type="submit" value="Update">
            </form>
        </div>
    </div>
</body>
</html>