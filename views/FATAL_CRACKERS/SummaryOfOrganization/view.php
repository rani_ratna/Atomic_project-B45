<?php
require_once ("../../../vendor/autoload.php");
$objSummaryOfOrganization = new \App\SummaryOfOrganization\SummaryOfOrganization();
$objSummaryOfOrganization->setData($_GET);
$oneData = $objSummaryOfOrganization->view();
?>

<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>$Summary Of Organization</title>
    <link rel="stylesheet" href="../../../resource/bootstrap/css/bootstrap.min.css">
    <script src="../../../resource/bootstrap/js/bootstrap.min.js"></script>
    <link rel="stylesheet" href="../../../resource/css/style.css">
</head>
<body>
    <div class="container col-md-8 col-md-offset-2">
        <h2 class="text-center tophead">Summary Of Organization - Active List</h2>
        <?php
            echo "
                <table class='table table-striped table-bordered'>
                    <tr>
                        <th>ID :</th>
                        <td>$oneData->id</td>
                    </tr>
                    <tr>
                        <th>Name :</th>
                        <td>$oneData->name</td>
                    </tr>
                    <tr>
                        <th>Describes :</th>
                        <td>$oneData->describes</td>
                    </tr>
                </table>
            ";
        ?>
        <a href="index.php" class="btn tophead btn-block">GO BACK</a><br><br>
    </div>
</body>
</html>