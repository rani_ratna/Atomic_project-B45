<?php
require_once("../../../vendor/autoload.php");

$objSummaryOfOrganization = new \App\SummaryOfOrganization\SummaryOfOrganization();

if(($_POST['btn'] == "TrashAll")){
    $objSummaryOfOrganization->MultipleTrash($_POST);
} elseif(($_POST['btn'] == "DeleteAll")){
    $objSummaryOfOrganization->MultipleDelete($_POST);
}elseif(($_POST['btn'] == "RecoverAll")){
    $objSummaryOfOrganization->MultipleRecover($_POST);
}