<?php
require_once("../../../vendor/autoload.php");

$objBirthDay = new \App\BirthDay\BirthDay();
if(($_POST['btn'] == "TrashAll")){
    $objBirthDay->MultipleTrash($_POST);
} elseif(($_POST['btn'] == "DeleteAll")){
    $objBirthDay->MultipleDelete($_POST);
}elseif(($_POST['btn'] == "RecoverAll")){
    $objBirthDay->MultipleRecover($_POST);
}