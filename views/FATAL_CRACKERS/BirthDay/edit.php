<?php
require_once("../../../vendor/autoload.php");
$objBirthDay = new \App\BirthDay\BirthDay();
$objBirthDay->setData($_GET);
$oneData = $objBirthDay->view();
?>
<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Birth Day Edit</title>
    <link rel="stylesheet" href="../../../resource/bootstrap/css/bootstrap.min.css">
    <script src="../../../resource/bootstrap/js/bootstrap.min.js"></script>
    <script src="../../../resource/js/jquery-3.1.1.min.js"></script>
    <link rel="stylesheet" href="../../../resource/css/style.css">
</head>
<body>
<div class="container">
    <h2 class="text-center tophead">Update Form</h2>
    <div id="InputForm" class="col-md-8 col-md-offset-2">
        <form role="form" action="update.php" method="post" class="form">
            <div class="form-group">
                <label for="name">Enter Name &nbsp;&nbsp;&nbsp;&nbsp; :</label>
                <input class="form-control" type="text" name="name" placeholder="  book name" value="<?php echo $oneData->name ?>" required>
            </div>
            <div class="form-group">
                <label for="authorName">Enter BirthDay :</label>
                <input class="form-control" type="date" name="dob" placeholder="  birth day" value="<?php echo $oneData->birth_day ?>" required>
            </div>
            <input type="hidden" name="id" value="<?php echo $oneData->id ?>">
            <input type="submit" value="Update">
        </form>
    </div>
</div>
</body>
</html>