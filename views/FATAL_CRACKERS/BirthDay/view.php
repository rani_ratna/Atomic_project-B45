<?php
require_once("../../../vendor/autoload.php");
$objBirthDay = new \App\BirthDay\BirthDay();
$objBirthDay->setData($_GET);
$oneData = $objBirthDay->view();
?>

<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Birth Day - Active List</title>
    <title>Birth Day - Active List</title>
    <link rel="stylesheet" href="../../../resource/bootstrap/css/bootstrap.min.css">
    <script src="../../../resource/bootstrap/js/bootstrap.min.js"></script>
    <link rel="stylesheet" href="../../../resource/css/style.css">
</head>
<body>
    <div class="container col-md-8 col-md-offset-2">
        <h2 class="text-center tophead">Birth Day - Active List</h2>
        <?php
            echo "
                <table class='table table-striped table-bordered'>
                    <tr>
                        <th>ID :</th>
                        <td>$oneData->id</td>
                    </tr>
                    <tr>
                        <th>Name :</th>
                        <td>$oneData->name</td>
                    </tr>
                    <tr>
                        <th>Birth Day :</th>
                        <td>$oneData->birth_day</td>
                    </tr>
                </table>
            ";
        ?>
        <a href="index.php" class="btn tophead btn-block">GO BACK</a><br><br>
    </div>
</body>
</html>