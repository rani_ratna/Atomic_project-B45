<?php
require_once("../../../vendor/autoload.php");

$objBookTitle = new App\BookTitle\BookTitile();

if(($_POST['btn'] == "TrashAll")){
    $objBookTitle->MultipleTrash($_POST);
} elseif(($_POST['btn'] == "DeleteAll")){
    $objBookTitle->MultipleDelete($_POST);
}elseif(($_POST['btn'] == "RecoverAll")){
    $objBookTitle->MultipleRecover($_POST);
}