<?php
require_once ("../../../vendor/autoload.php");
$objBookTitle = new \App\BookTitle\BookTitile();
$objBookTitle->setData($_GET);
$oneData = $objBookTitle->view();
?>
<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Book Title</title>
    <link rel="stylesheet" href="../../../resource/bootstrap/css/bootstrap.min.css">
    <script src="../../../resource/bootstrap/js/bootstrap.min.js"></script>
    <link type="text/css" rel="stylesheet" href="../../../resource/css/style.css">
</head>
<body>
    <div class="container">
        <div id="InputForm" class="col-md-8 col-md-offset-2">
            <form action="update.php" method="post" class="form">
                 <label>Enter Book Name&nbsp;&nbsp;&nbsp;&nbsp;:</label>
                 <input type="text" name="bookName" placeholder="  book name" value="<?php echo $oneData->book_name?>" required><br><br>
                 <label>Enter Author Name:</label>
                 <input type="text" name="authorName" placeholder="  author name" value="<?php echo $oneData->author_name?>" required><br><br>
                 <input type="hidden" name="id" value="<?php echo $oneData->id?>">
                 <input type="submit" value="Update">
            </form>
        </div>
    </div>
</body>
</html>

