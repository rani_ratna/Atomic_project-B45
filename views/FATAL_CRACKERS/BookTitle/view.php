<?php
require_once ("../../../vendor/autoload.php");
$objBookTitle = new App\BookTitle\BookTitile();
$objBookTitle->setData($_GET);
$oneData = $objBookTitle->view();
?>

<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Book Title</title>
    <title>Birth Day - Active List</title>
    <link rel="stylesheet" href="../../../resource/bootstrap/css/bootstrap.min.css">
    <script src="../../../resource/bootstrap/js/bootstrap.min.js"></script>
    <link rel="stylesheet" href="../../../resource/css/style.css">
</head>
<body>
    <div class="container col-md-8 col-md-offset-2">
        <h2 class="text-center tophead">Book Title - Active List</h2>
        <?php
            echo "
                <table class='table table-striped table-bordered'>
                    <tr>
                        <th>ID :</th>
                        <td>$oneData->id</td>
                    </tr>
                    <tr>
                        <th>Book Name :</th>
                        <td>$oneData->book_name</td>
                    </tr>
                    <tr>
                        <th>Author Name :</th>
                        <td>$oneData->author_name</td>
                    </tr>
                </table>
            ";
        ?>
        <a href="index.php" class="btn tophead btn-block">GO BACK</a><br><br>
    </div>
</body>
</html>