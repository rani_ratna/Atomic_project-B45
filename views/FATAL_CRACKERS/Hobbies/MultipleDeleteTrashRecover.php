<?php
require_once("../../../vendor/autoload.php");

$objHobbies = new \App\Hobbies\Hobbies();

if(($_POST['btn'] == "TrashAll")){
    $objHobbies->MultipleTrash($_POST);
} elseif(($_POST['btn'] == "DeleteAll")){
    $objHobbies->MultipleDelete($_POST);
}elseif(($_POST['btn'] == "RecoverAll")){
    $objHobbies->MultipleRecover($_POST);
}