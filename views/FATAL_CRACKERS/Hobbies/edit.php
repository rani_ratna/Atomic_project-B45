<?php
require_once ("../../../vendor/autoload.php");
$objHobbies = new \App\Hobbies\Hobbies();
$objHobbies->setData($_GET);
$oneData = $objHobbies->view();
$allHobbies = explode(",", $oneData->hobbies);
?>
<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Hobbies</title>
    <link rel="stylesheet" href="../../../resource/bootstrap/css/bootstrap.min.css">
    <script src="../../../resource/bootstrap/js/bootstrap.min.js"></script>
    <link rel="stylesheet" href="../../../resource/css/style.css">
</head>
<body>
    <div class="container">
        <div id="InputForm" class="col-md-8 col-md-offset-2">
            <form action="update.php" method="post" class="form">
                <label>Enter Your Name &nbsp;&nbsp;&nbsp;&nbsp; : </label>
                <input type="text" name="name" value="<?php echo $oneData->name?>" required><br><br>
                <label>Enter Your Hobbies  </label><br>
                <input type="checkbox" name="hobbie1" value="Programming" <?php
                    foreach($allHobbies as $oneHobbie){
                        if($oneHobbie == "Programming")echo "checked";
                    }
                ?>> Programming  &nbsp;&nbsp;
                <input type="checkbox" name="hobbie2" value="Drawing" <?php
                    foreach($allHobbies as $oneHobbie){
                        if($oneHobbie == "Drawing")echo "checked";
                    }
                ?>> Drawing  &nbsp;&nbsp;
                <input type="checkbox" name="hobbie3" value="Facebooking" <?php
                    foreach($allHobbies as $oneHobbie){
                        if($oneHobbie == "Facebooking")echo "checked";
                    }
                ?>> Facebooking<br><br>
                <input type="hidden" name="id" value="<?php echo $oneData->id?>">
                <input type="submit" value="Update">
            </form>
        </div>
    </div>
</body>
</html>